//aula sobre variáveis primitivias
//as variáveis primitivas são: string, number, undefined, null boolean
//exemplo:

const nome = 'shote';//string
const nome1 = 'shotinho';//string
const nome2 = 'waston';//string
const num = 10;//number
const num2 = 10.51;//number
let nomeAluno;//undefined -> Não aponta para local nenhum na memoria
const sobrenomeAluno = null;//null Não aponta para local nenhum na memoria
const aprovado = false;//bollean
