//let nome = 'shotinho';

let nome;

nome = 'shotinho';

console.log(nome, 'nasceu em 1983.');
console.log('Em 2000', nome, 'conheceu Maria');
console.log(nome, 'casou-se com Maria em 2014');
console.log('Maria teve 1 filho com', nome, 'em 2025');
console.log('O filho de', nome, 'se chama Shoshotinho');

//observacoes do curso
// Nao podemos criar variaveis com palavras reservadas
// Variaveis porecisam ter nomes significativos
// Nao pode comecar nome de variavel com um numero
// Utilizamos CamelCaser
// Variaveis no JS e case sensitive
// Variavies nao podem ser redeclaradas.
// Nao utilizar VAR utilizar LET para declarar as variaveis




