/* Exercicio da aula 8: Calcular o indice de massa corporal:
Washington tem 40 anos, peso de 95kg tem 1.9 de altura e seu IMC é de: 090904930490394 Washington nasceu em 1984*/

/* minha versão
const nome = 'Washington';
const sobrenome = 'Shotinho';
const idade = 40;
const peso = 84;
const altura = 1.8;

console.log('Calculo do IMC:');
console.log('Washington tem ', idade, 'seu peso é de: ', peso);

let imc = peso / (altura * altura);
let anoNasci = 2024 - idade;

console.log('seu IMC é de: ', imc);
console.log('seu ano de nascimento é:', anoNasci); */

//versão do curso

const nome = 'Washington Filho';
const sobremone = 'Filho';
const idade = 40;
const peso = 95;
const alturaEmCm = 1.89
let indiceMassaCorporal;
let anoNascimento;

indiceMassaCorporal = peso / (alturaEmCm * alturaEmCm);
anoNascimento = 2024 - idade;

console.log(`${nome} ${sobremone} tem ${idade} anos, pesa ${peso} kg`);
console.log(`tem ${alturaEmCm} de altura e seu IMC é de ${indiceMassaCorporal}`);
console.log(`${nome} nasceu em ${anoNascimento}`);

