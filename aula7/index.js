// Observacoes do curso
// Nao podemos criar constantes com palavras reservadas
// Constantes precisam ter nomes significativos
// Nao pode comecar nome de Constante com um numero
// Utilizamos CamelCaser
// Constantes no JS sao case sensitive
// Constantes nao podem ser redeclaradas.
// Nao utilizar VAR utilizar LET para declarar as variaveis
// Constante precisam ser declaradas em sua criacao; Exmp.: const pi = 3.141592653

//exeplo do livro javascript

/*Exemplo 1*/
//a =10;

//funcaoUm = function(){
//    let a = 20;
//    console.log(a);
//};
//funcaoUm();

//funcaoDois = function(){
//    console.log(a);
//};
//funcaoDois();

//console.log(a);

/*exmeplo 2 
a = 10;

funcaoUm = function(){
    let a = 20;
    console.log(a);

};
funcaoUm();

funcaoDois = function(){

    a = 40;
    console.log(a);

};
funcaoDois();

console.log(a);
*/

/* Exmeplo 3 
a = 30;

funcaoUm = function(){
    a = 20;
    console.log(a);
};
funcaoUm();

console.log(a);
*/

//Exemplo 4

/*funcaoUm = function(){
    let a = 20;
    console.log(a);

};
funcaoUm();
console.log(a);
*/

/* Exemplo 5
funcaoUm = function(){
    let a = 20;
    console.log(a, 'funcaoUm');
    function funcaoAninhada(){
        let a = 40;
        console.log(a, 'funcaoAninhada');
    }
    funcaoAninhada();
};
funcaoUm();
*/

/*exemplo 6
a = 40;

funcaoUm = function(){
    console.log(a);
    let a = 10;
    console.log(a);
};
funcaoUm();
*/

/* Sempre é interessante declarar o valor das variáveis no começo da função. NO exemplo a cima mesmo existindo um valor de uma variável declarada globalmente, é 
valido o contexto da função, e uma variável 'a' ja tinha sido declarada, por este motivo o valor retornou 'undefined' dando erro na execução do código*/

/*como boa pratica de programação, mas não é mandatório, devemos sempre declarar as variáveis usando let, para que a variável esteja no contexto global, evitando
sobrescrição*/